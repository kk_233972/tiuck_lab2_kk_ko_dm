package pl.tiuck;

/** pl.tiuck.Main class created as example for laboratories needs */
public class Main {
  /**
   * Print a message to the console
   *
   * @param args optional arguments which do nothing
   */
  public static void main(String args[]) {}
}
