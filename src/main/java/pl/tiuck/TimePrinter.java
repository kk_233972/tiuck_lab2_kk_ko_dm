package pl.tiuck;

import java.time.LocalDateTime;

/** pl.tiuck.TimePrinter is a class intended for printing current time */
public class TimePrinter implements PrintCurrentTime {
  private static String TIME_FORMAT = "yyyy:MM:dd HH:mm:ss";

  /**
   * Prints current time
   *
   * @return a string with current time
   */
  public String print() {
    return LocalDateTime.now().toString();
  }
}
