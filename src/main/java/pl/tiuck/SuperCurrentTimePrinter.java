package pl.tiuck;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/** The class is responsible for displaying current date */
public class SuperCurrentTimePrinter implements PrintCurrentTime {

  private static final String TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
  /**
   * This method is used to print current time
   *
   * @return String This returns current date and time
   */
  @Override
  public String print() {
    return LocalDateTime.now().format(DateTimeFormatter.ofPattern(TIME_FORMAT));
  }
}
