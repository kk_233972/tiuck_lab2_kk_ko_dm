package pl.tiuck;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/** The pl.tiuck.CurrentTime class provide method for return current time */
public class CurrentTime implements PrintCurrentTime {

  /** static String for time format */
  private static String TIME_FORMAT = "yyyy:MM:dd HH:mm:ss";

  /**
   * Method to get current time
   *
   * @return current time as String
   */
  public String print() {
    return new SimpleDateFormat(TIME_FORMAT).format(Calendar.getInstance().getTime());
  }
}
