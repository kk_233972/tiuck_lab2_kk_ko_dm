package printer.factory.kk;

import pl.tiuck.CurrentTime;
import pl.tiuck.PrintCurrentTime;
import pl.tiuck.SuperCurrentTimePrinter;
import pl.tiuck.TimePrinter;

/** printer.factory.kk is a class which allow to choose time printer  */
public class PrinterFactory {
  private CurrentTime currentTime = new CurrentTime();
  private SuperCurrentTimePrinter superCurrentTimePrinter = new SuperCurrentTimePrinter();
  private TimePrinter timePrinter = new TimePrinter();

    /**
     *
     * @param selectedPrinter define a printer type which you want to get
     * @return a class which is one of PrintCurrentTime interfaces implementation
     */
  public PrintCurrentTime getPrinter(PrinterType selectedPrinter) {
    switch (selectedPrinter) {
      case DM:
        return this.superCurrentTimePrinter;
      case KK:
        return this.timePrinter;
      case KO:
        return this.currentTime;
      default:
        throw new Error("Not supported printer type");
    }
  }
}
