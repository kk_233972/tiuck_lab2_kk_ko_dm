package pl.tiuck;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TimePrinterTest {

  @Test
  public void printShouldNOTReturnNull() {
    // arrange
    PrintCurrentTime printCurrentTime = new TimePrinter();
    // act
    String currentTime = printCurrentTime.print();
    // assert
    assertNotNull(currentTime);
  }
}
