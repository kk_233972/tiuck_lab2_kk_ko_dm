package printer.factory.kk;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import pl.tiuck.CurrentTime;
import pl.tiuck.PrintCurrentTime;
import pl.tiuck.SuperCurrentTimePrinter;
import pl.tiuck.TimePrinter;

public class PrinterFactoryTest {
  private PrinterFactory printerFactory;

  @Before
  public void createFactory() {
    this.printerFactory = new PrinterFactory();
  }

  @Test
  public void factoryShouldReturnSuperCurrentTimePrinter() {
    // arrange
    PrinterType printerType = PrinterType.DM;
    // act
    PrintCurrentTime currentTime = this.printerFactory.getPrinter(printerType);
    // assert
    assertTrue(currentTime instanceof SuperCurrentTimePrinter);
  }

  @Test
  public void factoryShouldReturnCurrentTime() {
    // arrange
    PrinterType printerType = PrinterType.KO;
    // act
    PrintCurrentTime currentTime = this.printerFactory.getPrinter(printerType);
    // assert
    assertTrue(currentTime instanceof CurrentTime);
  }

  @Test
  public void factoryShouldReturnTimePrinter() {
    // arrange
    PrinterType printerType = PrinterType.KK;
    // act
    PrintCurrentTime currentTime = this.printerFactory.getPrinter(printerType);
    // assert
    assertTrue(currentTime instanceof TimePrinter);
  }
}
